### What is NewsApp? ###

A simple Android application that serves as a news article client.  
News data is fetched from https://newsapi.org/ API.  
Data is refreshed automatically, and you can manually refresh it too.


### Main Design Principles ###

* Language: Kotlin
* Google Architecture Components: MVVM architecture + LiveData + Room database
* Kotlin Coroutines to handle asynchronous operations
* Retrofit 2 for networking
* Dagger 2 for dependency injection
* Room database as the single source of truth for UI data, so that the app can work offline
* Basic JUnit unit tests + basic Espresso UI tests - both use Mockito for mock creation