package com.aviv.newsapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.aviv.newsapp.api.NewsRemoteDataSource
import com.aviv.newsapp.data.Result
import com.aviv.newsapp.data.resultLiveData
import com.aviv.newsapp.db.dao.ArticleDao
import com.aviv.newsapp.db.entity.NewsArticle
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class NewsRepository @Inject constructor(
    private val newsRemoteDataSource: NewsRemoteDataSource,
    private val articleDao: ArticleDao,
    private val coroutineContext: CoroutineContext
) {

    private val articleData: MediatorLiveData<Result<List<NewsArticle>>> =
        MediatorLiveData()
    private var _articleData: LiveData<Result<List<NewsArticle>>> =
        MutableLiveData()

    fun loadArticles(): LiveData<Result<List<NewsArticle>>> {
        addSourceToMediator()
        return articleData
    }

    fun refreshData() {
        articleData.removeSource(_articleData)
        addSourceToMediator()
    }

    private fun addSourceToMediator() {
        _articleData = getArticlesLiveData()
        articleData.addSource(_articleData) { articleData.value = it }
    }

    private fun getArticlesLiveData() = resultLiveData(
        databaseQuery = { articleDao.getAll() },
        networkCall = { newsRemoteDataSource.getTopHeadlines() },
        saveCallResult = { articleDao.deleteAll(); articleDao.insertAll(*it.articles.toTypedArray()) },
        coroutineContext = coroutineContext
    )
}