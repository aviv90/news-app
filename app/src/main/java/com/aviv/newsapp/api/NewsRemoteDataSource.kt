package com.aviv.newsapp.api

import com.aviv.newsapp.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class NewsRemoteDataSource @Inject constructor(private val newsService: NewsService) : BaseRemoteDataSource() {

    suspend fun getTopHeadlines() = getResult { newsService.getTopHeadlines(country = "il") }
}