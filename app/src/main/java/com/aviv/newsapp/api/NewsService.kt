package com.aviv.newsapp.api

import com.aviv.newsapp.BuildConfig
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

const val BASE_URL = "https://newsapi.org/v2/"
const val DEFAULT_SOURCE = "cnn"
const val DEFAULT_COUNTRY = "us"

interface NewsService {

    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("source") source: String = DEFAULT_SOURCE,
        @Query("country") country: String = DEFAULT_COUNTRY,
        @Query("apiKey") apiKey: String = BuildConfig.API_TOKEN
    ): Call<TopHeadlinesResponse>
}