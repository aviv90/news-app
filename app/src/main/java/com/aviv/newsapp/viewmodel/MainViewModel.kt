package com.aviv.newsapp.viewmodel

import androidx.lifecycle.ViewModel
import com.aviv.newsapp.repository.NewsRepository
import com.aviv.newsapp.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class MainViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {

    fun loadArticles() = newsRepository.loadArticles()

    fun onActivityResumed() {
        newsRepository.refreshData()
    }

    fun onRefresh() {
        newsRepository.refreshData()
    }
}