package com.aviv.newsapp.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aviv.newsapp.db.entity.NewsArticle

@Dao
abstract class ArticleDao {

    @Query("SELECT * FROM article")
    abstract fun getAll(): LiveData<List<NewsArticle>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(vararg newsArticles: NewsArticle)

    @Query("DELETE FROM article")
    abstract fun deleteAll()
}