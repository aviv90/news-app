package com.aviv.newsapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aviv.newsapp.db.dao.ArticleDao
import com.aviv.newsapp.db.entity.NewsArticle

@Database(
    entities = [NewsArticle::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class NewsDb : RoomDatabase() {

    abstract fun articleDao(): ArticleDao
}