package com.aviv.newsapp.di.component

import android.app.Application
import com.aviv.newsapp.NewsApp
import com.aviv.newsapp.di.module.AppModule
import com.aviv.newsapp.di.module.MainActivityModule
import com.aviv.newsapp.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ViewModelModule::class,
    MainActivityModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(newsApp: NewsApp)
}