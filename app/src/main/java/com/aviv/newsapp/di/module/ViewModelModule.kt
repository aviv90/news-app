package com.aviv.newsapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aviv.newsapp.di.ViewModelKey
import com.aviv.newsapp.viewmodel.ArticleViewModel
import com.aviv.newsapp.viewmodel.MainViewModel
import com.aviv.newsapp.viewmodel.NewsAppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: NewsAppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel
}