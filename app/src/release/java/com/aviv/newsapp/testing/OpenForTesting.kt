package com.aviv.newsapp.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting