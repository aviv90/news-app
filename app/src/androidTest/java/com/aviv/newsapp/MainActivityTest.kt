package com.aviv.newsapp

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.aviv.newsapp.data.Result
import com.aviv.newsapp.di.module.TestViewModelModule
import com.aviv.newsapp.db.entity.NewsArticle
import com.aviv.newsapp.ui.MainActivity
import com.aviv.newsapp.util.AndroidTestUtil
import com.aviv.newsapp.util.SwipeRefreshLayoutMatchers
import com.aviv.newsapp.viewmodel.MainViewModel
import it.xabaras.android.espresso.recyclerviewchildactions.RecyclerViewChildActions
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.`when`

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private lateinit var viewModel: MainViewModel
    private val articleData = MutableLiveData<Result<List<NewsArticle>>>()

    @get:Rule
    val activityRule = object : ActivityTestRule<MainActivity>(
        MainActivity::class.java,
        false,
        true
    ) {
        override fun beforeActivityLaunched() {
            super.beforeActivityLaunched()
            viewModel = Mockito.mock(MainViewModel::class.java)
            `when`(TestViewModelModule.viewModelFactory.create(MainViewModel::class.java)).thenReturn(
                viewModel
            )
            `when`(viewModel.loadArticles()).thenReturn(articleData)
        }
    }

    @Test
    fun loading() {
        articleData.postValue(AndroidTestUtil.createFakeLoadingResult())
        onView(withId(R.id.swipeRefreshMain))
            .check(matches(SwipeRefreshLayoutMatchers.isRefreshing()))
    }

    @Test
    fun success() {
        articleData.postValue(AndroidTestUtil.createFakeSuccessResult())
        onView(withId(R.id.recyclerMain)).check(matches(hasChildCount(3)))
        onView(withId(R.id.recyclerMain))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))
        onView(withId(R.id.recyclerMain))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.titleText,
                        2,
                        withText("title3")
                    )
                )
            )
    }

    @Test
    fun failure() {
        articleData.postValue(AndroidTestUtil.createFakeFailureResult())
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.error_loading_news_text)))
    }
}