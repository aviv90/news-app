package com.aviv.newsapp.di.component

import android.app.Application
import com.aviv.newsapp.TestApp
import com.aviv.newsapp.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, TestViewModelModule::class,
    MainActivityModule::class])
interface TestAppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): TestAppComponent
    }

    fun inject(testApp: TestApp)
}