package com.aviv.newsapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.aviv.newsapp.api.NewsRemoteDataSource
import com.aviv.newsapp.data.Result
import com.aviv.newsapp.db.dao.ArticleDao
import com.aviv.newsapp.db.entity.NewsArticle
import com.aviv.newsapp.repository.NewsRepository
import com.aviv.newsapp.util.CoroutineTestRule
import com.aviv.newsapp.util.TestUtil
import com.aviv.newsapp.util.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class NewsRepositoryTest {

    private val articleDao = mock(ArticleDao::class.java)
    private val newsDataSource = mock(NewsRemoteDataSource::class.java)
    private val testDispatcher = TestCoroutineDispatcher()
    private val repo = NewsRepository(newsDataSource, articleDao, testDispatcher)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val coroutineTestRule = CoroutineTestRule(testDispatcher)

    @Test
    fun goToNetwork() {
        runBlockingTest {
            val successResult = TestUtil.createFakeSuccessResult()
            `when`(newsDataSource.getTopHeadlines()).thenReturn(successResult)
            val dbLiveData = MutableLiveData<List<NewsArticle>>()
            `when`(articleDao.getAll()).thenReturn(dbLiveData)

            val repoLiveData = repo.loadArticles()
            val observer = mock<Observer<Result<List<NewsArticle>>>>()
            repoLiveData.observeForever(observer)

            verify(articleDao).deleteAll()
            verify(articleDao).insertAll(*successResult.data!!.articles.toTypedArray())
            verify(articleDao).getAll()
        }
    }

    @Test
    fun dontGoToNetwork() {
        runBlockingTest {
            val failureResult = TestUtil.createFakeFailureResult()
            `when`(newsDataSource.getTopHeadlines()).thenReturn(failureResult)
            val dbLiveData = MutableLiveData<List<NewsArticle>>()
            `when`(articleDao.getAll()).thenReturn(dbLiveData)

            val repoLiveData = repo.loadArticles()
            val observer = mock<Observer<Result<List<NewsArticle>>>>()
            repoLiveData.observeForever(observer)

            verify(articleDao, never()).deleteAll()
            verify(articleDao).getAll()
        }
    }
}